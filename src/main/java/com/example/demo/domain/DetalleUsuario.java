package com.example.demo.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "detalleUsuario")
@Getter
@Setter
public class DetalleUsuario {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(columnDefinition = "serial")
    private int id;

    private String ADP;

    private String nombre;

    private  String apellido;

    private String cedula;

    private String cargo;

    private String direccion;

    private String celular;

    @ManyToOne
    private Campaña campaña;




}
