package com.example.demo.web.rest;

import com.example.demo.domain.Equipo;
import com.example.demo.service.EquipoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class EquipoResource {

    @Autowired
    EquipoService equipoService;

    @PostMapping("/equipo")
    public Equipo create(@RequestBody Equipo equipo){
        return equipoService.create(equipo);
    }

    @GetMapping("/equipo")
    public Iterable<Equipo>read(){
        return equipoService.read();
    }

    @PutMapping("/equipo")
    public Equipo update(@RequestBody Equipo equipo){

        return equipoService.update(equipo);
    }

    @DeleteMapping("/equipo/{id}")
    public void delete(Integer id){

        equipoService.detele(id);
    }
}
