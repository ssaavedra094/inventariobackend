package com.example.demo.web.rest;

import com.example.demo.domain.Pantalla;
import com.example.demo.service.PantallaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class PantallaResource {

    @Autowired
    PantallaService pantallaService;

    @PostMapping("/pantalla")
    public Pantalla create(@RequestBody Pantalla pantalla){

        return pantallaService.create(pantalla);
    }

    @GetMapping("/pantalla")
    public Iterable<Pantalla>read(){
        return pantallaService.read();
    }

    @DeleteMapping("/pantalla/{id}")
    public void delete(@PathVariable Integer id){

        pantallaService.delete(id);
    }

    @PutMapping
    public Pantalla update(@RequestBody Pantalla pantalla){

        return pantallaService.update(pantalla);
    }
}
