package com.example.demo.web.rest;

import com.example.demo.domain.Campaña;
import com.example.demo.service.CampañaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/api")
@RestController
public class CampañaResource {

    @Autowired
    CampañaService campañaService;

    @GetMapping("/campaña")
    public Iterable<Campaña>read(){

        return campañaService.read();
    }

    @PostMapping("/campaña")
    public Campaña create(@RequestBody Campaña campaña){

        return campañaService.create(campaña);
    }

    @PutMapping("/campaña")
    public Campaña update(@RequestBody Campaña campaña){

        return  campañaService.update(campaña);
    }
    @DeleteMapping("/campaña/{id}")
    public void delete(Integer id){
        campañaService.detele(id);
    }
}
