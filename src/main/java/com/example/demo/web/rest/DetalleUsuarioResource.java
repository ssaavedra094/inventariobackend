package com.example.demo.web.rest;

import com.example.demo.domain.DetalleUsuario;
import com.example.demo.service.DetalleUsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class DetalleUsuarioResource {

    @Autowired
    DetalleUsuarioService detalleUsuarioService;

    @GetMapping("/detalleUsuario")
    public Iterable<DetalleUsuario> read(){
        return  detalleUsuarioService.read();
    }

    @PostMapping("/detalleUsuario")
    public DetalleUsuario create(@RequestBody DetalleUsuario detalleUsuario){
        return detalleUsuarioService.create(detalleUsuario);
    }

    @PutMapping("/detalleUsuario")
    public DetalleUsuario update(@RequestBody DetalleUsuario detalleUsuario){
        return detalleUsuarioService.update(detalleUsuario);
    }

    @DeleteMapping("/detalleUsuario/{id}")
    public void delete(@PathVariable Integer id){
        detalleUsuarioService.delete(id);
    }
}
