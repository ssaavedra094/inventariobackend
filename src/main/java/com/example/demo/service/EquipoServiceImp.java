package com.example.demo.service;

import com.example.demo.domain.Equipo;
import com.example.demo.repository.EquipoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EquipoServiceImp implements EquipoService{

    @Autowired
    EquipoRepository equipoRepository;


    @Override
    public Equipo create(Equipo equipo) {
        return equipoRepository.save(equipo);
    }

    @Override
    public Iterable<Equipo> read() {
        return equipoRepository.findAll();
    }

    @Override
    public void detele(Integer id) {
        equipoRepository.deleteById(id);
    }

    @Override
    public Equipo update(Equipo equipo) {
        return equipoRepository.save(equipo);
    }
}
