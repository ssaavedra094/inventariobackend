package com.example.demo.service;

import com.example.demo.domain.DetalleUsuario;
import com.example.demo.repository.DetalleUsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DetalleUsuarioServiceImp implements DetalleUsuarioService{

    @Autowired
    DetalleUsuarioRepository detalleUsuarioRepository;


    @Override
    public Iterable<DetalleUsuario> read() {
        return detalleUsuarioRepository.findAll();
    }

    @Override
    public DetalleUsuario update(DetalleUsuario detalleUsuario) {
        return detalleUsuarioRepository.save(detalleUsuario);
    }

    @Override
    public DetalleUsuario create(DetalleUsuario detalleUsuario) {
        return detalleUsuarioRepository.save(detalleUsuario);
    }

    @Override
    public void delete(Integer id) {
        detalleUsuarioRepository.deleteById(id);
    }
}
