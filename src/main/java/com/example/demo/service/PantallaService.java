package com.example.demo.service;

import com.example.demo.domain.Pantalla;

public interface PantallaService {

    public Pantalla create(Pantalla pantalla);

    public Iterable<Pantalla>read();

    public void delete(Integer id);

    public Pantalla update(Pantalla pantalla);

}
