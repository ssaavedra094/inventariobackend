package com.example.demo.service;

import com.example.demo.domain.Campaña;

public interface CampañaService {

    public Campaña create(Campaña campaña);

    public void detele(Integer id);

    public Campaña update(Campaña campaña);

    public Iterable<Campaña> read();
}
