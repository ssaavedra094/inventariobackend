package com.example.demo.service;

import com.example.demo.domain.Campaña;
import com.example.demo.repository.CampañaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CampañaServiceImp implements CampañaService{

    @Autowired
    CampañaRepository campañaRepository;


    @Override
    public Campaña create(Campaña campaña) {
        return campañaRepository.save(campaña);
    }

    @Override
    public void detele(Integer id) {
        campañaRepository.deleteById(id);
    }

    @Override
    public Campaña update(Campaña campaña) {
        return campañaRepository.save(campaña);
    }

    @Override
    public Iterable<Campaña> read() {
        return campañaRepository.findAll();
    }
}
