package com.example.demo.service;

import com.example.demo.domain.DetalleUsuario;

public interface DetalleUsuarioService {

    public Iterable<DetalleUsuario>read();

    public DetalleUsuario update(DetalleUsuario detalleUsuario);

    public DetalleUsuario create(DetalleUsuario detalleUsuario);

    public void delete(Integer id);
}
