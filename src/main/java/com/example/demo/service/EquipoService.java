package com.example.demo.service;

import com.example.demo.domain.Equipo;

public interface EquipoService {

    public Equipo create(Equipo quipo);

    public Iterable<Equipo>read();

    public void detele(Integer id);

    public Equipo update(Equipo equipo);
}
