package com.example.demo.service;

import com.example.demo.domain.Pantalla;
import com.example.demo.repository.PantallaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PantallaServiceImp implements PantallaService{

    @Autowired
    PantallaRepository pantallaRepository;

    @Override
    public Pantalla create(Pantalla pantalla) {
        return pantallaRepository.save(pantalla);
    }

    @Override
    public Iterable<Pantalla> read() {
        return pantallaRepository.findAll();
    }

    @Override
    public void delete(Integer id) {
        pantallaRepository.deleteById(id);

    }

    @Override
    public Pantalla update(Pantalla pantalla) {
        return pantallaRepository.save(pantalla);
    }
}
