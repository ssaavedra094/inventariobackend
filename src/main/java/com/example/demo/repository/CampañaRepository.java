package com.example.demo.repository;

import com.example.demo.domain.Campaña;
import org.springframework.data.repository.CrudRepository;

public interface CampañaRepository extends CrudRepository<Campaña,Integer> {
}
