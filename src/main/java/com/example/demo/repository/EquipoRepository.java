package com.example.demo.repository;

import com.example.demo.domain.Equipo;
import org.springframework.data.repository.CrudRepository;

public interface EquipoRepository extends CrudRepository<Equipo, Integer> {
}
