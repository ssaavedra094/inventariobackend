package com.example.demo.repository;

import com.example.demo.domain.DetalleUsuario;
import org.springframework.data.repository.CrudRepository;

public interface DetalleUsuarioRepository extends CrudRepository<DetalleUsuario, Integer> {
}
