package com.example.demo.repository;

import com.example.demo.domain.Pantalla;
import io.swagger.models.auth.In;
import org.springframework.data.repository.CrudRepository;

public interface PantallaRepository extends CrudRepository<Pantalla,Integer> {
}
